using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lookaround : MonoBehaviour
{

    float mouseX;
    float mouseY;

    public Transform cameraTransform;

    public float mouseSensitivity = 100f;

    private float yRotation;
    private float xRotation;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        cameraTransform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        yRotation += mouseX;
        gameObject.transform.rotation = Quaternion.Euler(0, yRotation, 0);
    }
}
