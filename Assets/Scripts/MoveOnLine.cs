using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnLine : MonoBehaviour
{
    public List<GameObject> waypoints;
    public float speed = 2;
    int index = 0;
    void Start()
    {
        
    }

    private void Update()
    {
        Vector3 destination = waypoints[index].transform.position;
        Vector3 newPos = Vector3.MoveTowards(transform.position, waypoints[index].transform.position, (speed * Time.deltaTime)/2);
        transform.position = newPos;

        float distance = Vector3.Distance(transform.position, destination);
        if(distance <= 0.05)
        {
            index++;
        }
    }
}
