using System.Collections;
using UnityEngine;

public class Day_Night_Cycle : MonoBehaviour
{

    public Vector3 RotationAxis = Vector3.right;        //x axis
    Quaternion _startRotation;
    float _rotationIncrement = 0;
    float time;
    float timeDelay;

    void Start()                        // alle Startvariablen initialisieren
    {
        _startRotation = transform.rotation;
        time = 0f;
        timeDelay = 0.1f;
    }

    // Update is called once per frame
    void Update()           // rotierung der dynamischen Belichtung nur in der x axis
    {
        time = time + 1f * Time.deltaTime;
        if (time >= timeDelay)
        {
            time = 0f;
            Quaternion rotationMod = Quaternion.AngleAxis(_rotationIncrement, RotationAxis);
            _rotationIncrement += 1;
            transform.rotation = _startRotation * rotationMod;
        }
    }
}

