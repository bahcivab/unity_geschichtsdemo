using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    private CharacterController _characterController;

    float horizontal_movement;
    float vertical_movement;

    public float movement_speed;

    float mouseY;
    float mouseX;

    public float mouseSensitivity = 100f;

    private float yRotation;
    private float xRotation;

    public Transform cameraTransform;

    Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        _characterController = gameObject.GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        horizontal_movement = Input.GetAxis("Horizontal");
        vertical_movement = Input.GetAxis("Vertical");

        moveDirection = gameObject.transform.forward * vertical_movement + gameObject.transform.right * horizontal_movement;  //Aktuelle Bewegungsrichtung


        _characterController.SimpleMove(moveDirection * movement_speed);                // Controller mit Physik
                                                                                        // gameObject.transform.Translate(moveDirection * movement_speed * Time.deltaTime);  // Ohne Physik
        mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        cameraTransform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        yRotation += mouseX;
        gameObject.transform.rotation = Quaternion.Euler(0, yRotation, 0);

    }
}
